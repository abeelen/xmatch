XMatch
======

|pypi| |license| |wheels| |format| |pyversions| |build| |coverage| |rtd|

`xmatch` is used to cross match a sky position to a series of catalog, at once.

.. code:: bash

	  $ cross_match xmatch.cfg --lonlat 0.0 0.0 

or as a python module

.. code:: python

	  from astropy.coordinates import SkyCoord
	  import xmatch
	  ref = SkyCoord(0,0, unit="deg", frame="galactic")
	  cat_list = xmatch.parse_config('xmatch.cfg')['cat_fits']
	  result = xmatch.xmatch(ref, cat_list)


Features
--------

- Galactic and equatorial system supported
- Output in ``fits``, ``csv`` , ``txt`` or ``html`` for the central point source photometry

Installation
------------

Install ``xmatch`` using pip :

.. code:: bash

    $ pip install xmatch

or by running setuptools on `source <https://git.ias.u-psud.fr/abeelen/xmatch/tree/master>`_


.. code:: bash

    $ python setup.py install

Contribute
----------

- `Issues Tracker <https://git.ias.u-psud.fr/abeelen/xmatch/issues>`_
- `Source Code <https://git.ias.u-psud.fr/abeelen/xmatch/tree/master>`_

Support
-------

If you are having issues, please let us know.

License
-------

This project is licensed under the LGPL+3.0 license.

.. |pypi| image:: https://img.shields.io/pypi/v/xmatch.svg?maxAge=2592000
    :alt: Latest Version
    :target: https://pypi.python.org/pypi/xmatch

.. |license| image:: https://img.shields.io/pypi/l/xmatch.svg?maxAge=2592000
    :alt: License

.. |wheels| image:: https://img.shields.io/pypi/wheel/xmatch.svg?maxAge=2592000
   :alt: Wheels

.. |format| image:: https://img.shields.io/pypi/format/xmatch.svg?maxAge=2592000
   :alt: Format

.. |pyversions| image:: https://img.shields.io/pypi/pyversions/xmatch.svg?maxAge=2592000
   :alt: pyversions


.. |build| image:: https://git.ias.u-psud.fr/abeelen/xmatch/badges/master/build.svg
    :alt: Master Build
    :target: https://git.ias.u-psud.fr/abeelen/xmatch/builds

.. |coverage| image:: https://git.ias.u-psud.fr/abeelen/xmatch/badges/master/coverage.svg
    :alt: Master Coverage

.. |rtd| image:: https://readthedocs.org/projects/xmatch/badge/?version=latest
    :alt: Read the doc
    :target: http://xmatch.readthedocs.io/
