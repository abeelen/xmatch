#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (c) 2016 IAS / CNRS / Univ. Paris-Sud
# LGPL License - see attached LICENSE file
# Author: Alexandre Beelen <alexandre.beelen@ias.u-psud.fr>
from __future__ import unicode_literals

import logging
LOGGER = logging.getLogger('django')

import os
import pytest
import argparse

import numpy as np
from numpy import testing as npt

from astropy import units as u
from astropy import coordinates as coord
from astropy.table import Table

from .. import check_config_cat, check_config_vizier, parse_config, parse_args, combine_args
from .. import cross_match, xmatch_fits, merge_tables, main

try:  # pragma: py3
    PATTERN = None
    from configparser import RawConfigParser, ConfigParser, ExtendedInterpolation
except ImportError:  # pragma: py2
    import re
    from ConfigParser import RawConfigParser, ConfigParser
    PATTERN = re.compile(r"\$\{(.*?)\}")

try:  # pragma: py3
    from io import StringIO
except ImportError:  # pragma: py2
    from cStringIO import StringIO


try:  # pragma: py3
    FileNotFoundError
except NameError:  # pragma: py2
    FileNotFoundError = IOError


class TestCheckConfig:

    def test_check_config_cat_exceptions(self):
        config = ConfigParser()
        config.add_section('cat')

        opt_in = {'filename': 'filename',
                  'ext': 'ext',
                  'lon': 'lon',
                  'lat': 'lat',
                  'frame': 'frame'}
        for key in opt_in.keys():
            with pytest.raises(ValueError):
                opt_out = check_config_cat(config, 'cat')
            config.set('cat', key, opt_in[key])

        opt_out = check_config_cat(config, 'cat')

        for key in opt_in.keys():
            assert opt_out[key] == opt_in[key]

    def test_check_config_vizier_exceptions(self):
        config = ConfigParser()
        config.add_section('cat')

        opt_in = {'catalog': 'catalog',
                  'dist_threshold': '1.2'}
        for key in opt_in.keys():
            with pytest.raises(ValueError):
                opt_out = check_config_vizier(config, 'cat')
            config.set('cat', key, opt_in[key])


class TestParseConfig:

    def test_parse_config_empty(self, tmpdir_factory):
        # Insure empty file
        conffile = tmpdir_factory.mktemp("conf").join("xmatch.cfg")
        with pytest.raises(FileNotFoundError):
            config = parse_config(str(conffile))

    @pytest.mark.parametrize("ini_file",
                             [""" """,
                              """[global]""",
                              """[global]
                              cat_list:
                              """,
                              """[global]
                              cat_list: cat1
                              """,
                              """[global]
                              cat_list: cat1
                              [cat1]
                              filename: filename1,
                              ext: ext1,
                              lon: lon1,
                              lat: lat1,
                              frame: frame1
                              """,
                              """[global]
                              vizier_list: viz1
                              [viz1]
                              catalog: cat1
                              """])
    def test_parse_config_found(self, tmpdir_factory, ini_file):
        conffile = tmpdir_factory.mktemp("conf").join("xmatch.cfg")

        config = ConfigParser()
        config.read(StringIO(ini_file))

        config.write(conffile.open(mode="w", ensure=True))
        with pytest.raises(ValueError):
            config = parse_config(str(conffile))

    @pytest.fixture(scope='session')
    def generate_default_conffile(self, tmpdir_factory):
        conffile = tmpdir_factory.mktemp("conf").join("xmatch.cfg")
        config = ConfigParser()
        config.add_section('global')
        config.set('global', 'cat_list', 'cat1')
        config.add_section('cat1')
        config.set('cat1', 'filename', 'filename1')
        config.set('cat1', 'ext', 'ext1')
        config.set('cat1', 'lon', 'lon1')
        config.set('cat1', 'lat', 'lat1')
        config.set('cat1', 'fields', 'col1 col2')
        config.set('cat1', 'frame', 'frame1')
        config.set('cat1', 'dist_threshold', str(0.0))
        config.write(conffile.open(mode='w', ensure=True))
        return conffile, config

    def test_parse_config(self, generate_default_conffile):
        conffile, config = generate_default_conffile

        config = parse_config(str(conffile))
        assert config.get('input_tab') is None

        cat_fits = config.get('cat_fits')
        assert len(cat_fits) == 1
        assert isinstance(cat_fits[0], tuple)
        assert cat_fits[0][0] == 'cat1'
        assert cat_fits[0][1] == {'filename': 'filename1',
                                  'ext': 'ext1',
                                  'lon': 'lon1', 'lat': 'lat1',
                                  'frame': 'frame1',
                                  'fields': ['col1', 'col2'],
                                  'dist_threshold': 0.0}

    def test_parse_config_input(self, generate_default_conffile):
        conffile, config = generate_default_conffile
        config.add_section('input')
        config.set('input', 'filename', 'filename_input')
        config.set('input', 'ext', 'ext_input')
        config.set('input', 'lon', 'lon_input')
        config.set('input', 'lat', 'lat_input')
        config.set('input', 'frame', 'frame_input')
        config.write(conffile.open(mode='w', ensure=True))
        test_config = parse_config(str(conffile))
        assert test_config.get('input_tab') == \
            {'filename': 'filename_input',
             'ext': 'ext_input',
             'lon': 'lon_input', 'lat': 'lat_input',
             'frame': 'frame_input', 'fields': ['all']}

    def test_parse_config_output(self, generate_default_conffile):
        conffile, config = generate_default_conffile
        config.add_section('output')
        config.set('output', 'filename', 'filename_output')
        config.set('output', 'outdir', 'outdir')
        config.write(conffile.open(mode='w', ensure=True))

        test_config = parse_config(str(conffile))
        assert test_config.get('filename') == 'filename_output'
        assert test_config.get('outdir') == 'outdir'

    def test_parse_config_input_interpolation(self, generate_default_conffile):
        conffile, config = generate_default_conffile
        config.set('global', 'directory', 'indir')
        config.set('input', 'filename', '${global:directory}/filename_input')
        config.write(conffile.open(mode='w', ensure=True))
        test_config = parse_config(str(conffile))
        assert test_config.get('input_tab') == \
            {'filename': 'indir/filename_input',
             'ext': 'ext_input',
             'lon': 'lon_input', 'lat': 'lat_input',
             'frame': 'frame_input', 'fields': ['all']}

    def test_parse_config_cat_interpolation(self, generate_default_conffile):
        conffile, config = generate_default_conffile
        config.set('global', 'directory', 'indir')
        config.set('cat1', 'filename', '${global:directory}/filename1')
        config.write(conffile.open(mode='w', ensure=True))
        test_config = parse_config(str(conffile))
        cat_fits = test_config.get('cat_fits')
        assert cat_fits[0][1]['filename'] == 'indir/filename1'

    def test_parse_config_output_interpolation(self, generate_default_conffile):
        conffile, config = generate_default_conffile
        config.set('global', 'directory', 'indir')
        config.set('output', 'filename', '${global:directory}/filename_output')
        config.write(conffile.open(mode='w', ensure=True))
        test_config = parse_config(str(conffile))
        assert test_config.get('filename') == 'indir/filename_output'

    @pytest.mark.parametrize("verbosity, level",
                             [('verbose', logging.DEBUG),
                              ('debug', logging.DEBUG),
                              ('quiet', logging.ERROR),
                              ('error', logging.ERROR),
                              ('info', logging.INFO),
                              (str(logging.INFO), logging.INFO),
                              ('aze', None)
                             ])
    def test_parse_config_output_verbosity(self,
                                           generate_default_conffile,
                                           verbosity, level):
        conffile, config = generate_default_conffile
        config.set('output', 'verbosity', verbosity)
        config.write(conffile.open(mode='w', ensure=True))
        test_config = parse_config(str(conffile))

        assert test_config.get('verbosity') == level


class TestParseArgs:

    def test_parse_args_empty(self):
        with pytest.raises(SystemExit):
            args = parse_args(' '.split())

    def test_parse_args_defaults(self):
        args = parse_args('xmatch.cfg'.split())

        assert args.conf == 'xmatch.cfg'
        assert args.lonlat is None
        assert args.coordframe == 'galactic'
        assert args.filename is None
        assert args.outdir is None
        assert args.verbosity is None

    @pytest.mark.parametrize("verbosity, level",
                             [('', None),
                              ('--verbose', logging.DEBUG),
                              ('--quiet', logging.ERROR)
                             ])
    def test_parse_args_verbosity(self, verbosity, level):
        args = parse_args(('xmatch.cfg ' + verbosity).split())
        assert args.verbosity == level


class TestCombineArgs():

    def test_combine_args_exception(self):
        config = {}
        args = argparse.Namespace(lonlat=None, conf='xmatch.cfg')
        with pytest.raises(ValueError):
            coord_in, tab_in, cat_fits, cat_vizier, output = combine_args(
                args, config)

    def test_combine_args_lonlat(self):
        config = {}
        args = argparse.Namespace(lonlat=[0.0, 0.0], coordframe='icrs',
                                  verbosity=None, outdir=None,
                                  filename=None, conf='xmatch.cfg')
        coord_in, tab_in, cat_fits, cat_vizier, output = combine_args(
            args, config)
        assert isinstance(coord_in, coord.SkyCoord)
        assert coord_in.icrs.ra == 0
        assert coord_in.icrs.dec == 0
        assert tab_in.columns.keys() == ['index', 'glon', 'glat',
                                         'RA', 'DEC']
        assert len(tab_in) == 1
        assert tab_in['index'] == 0
        assert tab_in['RA'] == 0
        assert tab_in['DEC'] == 0

    def test_combine_args_input_tab(self, tmpdir_factory):

        input_tab = tmpdir_factory.mktemp("data").join("input_tab.fits")

        tab_in = Table()
        tab_in.add_column(Table.Column(name='index', data=[0]))
        tab_in.add_column(Table.Column(name='RA', data=[0]))
        tab_in.add_column(Table.Column(name='DEC', data=[0]))

        config = {'input_tab': {'filename': str(input_tab),
                                'ext': 1,
                                'lon': 'RA', 'lat': 'DEC',
                                'frame': 'icrs',
                                'fields': ['all']}}
        args = argparse.Namespace(lonlat=None, coordframe=None,
                                  verbosity=None, outdir=None,
                                  filename=None, conf='xmatch.cfg')

        with pytest.raises(IOError):
            coord_in, tab_in, cat_fits, cat_vizier, output = combine_args(
                args, config)

        tab_in.write(str(input_tab), format='fits')
        coord_in, tab_in, cat_fits, cat_vizier, output = combine_args(
            args, config)
        assert isinstance(coord_in, coord.SkyCoord)
        assert coord_in.icrs.ra == 0
        assert coord_in.icrs.dec == 0
        assert tab_in.columns.keys() == ['index', 'RA', 'DEC']
        assert len(tab_in) == 1
        assert tab_in['index'] == 0
        assert tab_in['RA'] == 0
        assert tab_in['DEC'] == 0

    @pytest.mark.parametrize("cmd, conf, level",
                             [('--verbose', {}, logging.DEBUG),
                              ('--quiet', {}, logging.ERROR),
                              ('', {
                                  'verbosity': logging.DEBUG}, logging.DEBUG),
                              ('--quiet', {
                                  'verbosity': logging.DEBUG}, logging.ERROR)
                             ])
    def test_combine_args_verbosity(self, cmd, conf, level):
        args = parse_args(('xmatch.cfg --lonlat 0.0 0.0 ' + cmd).split())
        coord_in, tab_in, cat_fits, cat_vizier, output = combine_args(
            args, conf)
        assert LOGGER.level == level

    @pytest.mark.parametrize("cmd, conf, result",
                             [('', {},
                               {'outdir': '.', 'filename': None}),
                              ('--outdir toto', {'filename': 'filename.fits'},
                               {'outdir': 'toto', 'filename': 'filename.fits'}),
                             ])
    def test_combine_args_output(self, cmd, conf, result):
        args = parse_args(('xmatch.cfg --lonlat 0.0 0.0 ' + cmd).split())
        coord_in, tab_in, cat_fits, cat_vizier, output = combine_args(
            args, conf)
        assert output == result


class TestCrossMatch:

    @pytest.fixture(scope='session')
    def generate_xmatchs(self, tmpdir_factory):
        data_dir = tmpdir_factory.mktemp("data")

        conffile = data_dir.join("xmatch.cfg")
        config = ConfigParser()
        config.add_section('global')
        config.set('global', 'cat_list', 'cat1 cat2 cat3')
        config.set('global', 'datadir', str(data_dir))

        cat_fits = []

        ras = np.linspace(0, 1, 11) * u.arcmin
        decs = np.linspace(0, 1, 11) * u.arcmin
        ra_v, dec_v = np.meshgrid(ras, decs)

        tab1_file = data_dir.join("tab1.fits")
        tab1 = Table()
        tab1.add_column(Table.Column(name='RA',
                                     data=ra_v.flatten().to(u.deg)))
        tab1.add_column(Table.Column(name='DEC',
                                     data=dec_v.flatten().to(u.deg)))
        tab1.write(str(tab1_file), format='fits')

        config.add_section('cat1')
        config.set('cat1', 'filename', '${global:datadir}/tab1.fits')
        config.set('cat1', 'ext', str(1))
        config.set('cat1', 'lon', 'RA')
        config.set('cat1', 'lat', 'DEC')
        config.set('cat1', 'frame', 'icrs')
        config.set('cat1', 'dist_threshold', str(0.1))

        ras = np.linspace(0, 1, 6) * u.arcmin
        decs = np.linspace(0, 1, 6) * u.arcmin
        ra_v, dec_v = np.meshgrid(ras, decs)

        tab2_file = data_dir.join("tab2.fits")
        tab2 = Table()
        tab2.add_column(Table.Column(name='RA',
                                     data=ra_v.flatten().to(u.deg)))
        tab2.add_column(Table.Column(name='DEC',
                                     data=dec_v.flatten().to(u.deg)))
        tab2.write(str(tab2_file), format='fits')

        config.add_section('cat2')
        config.set('cat2', 'filename', '${global:datadir}/tab2.fits')
        config.set('cat2', 'ext', str(1))
        config.set('cat2', 'lon', 'RA')
        config.set('cat2', 'lat', 'DEC')
        config.set('cat2', 'frame', 'icrs')
        config.set('cat2', 'dist_threshold', str(0.1))

        config.add_section('cat3')
        config.set('cat3', 'filename', '${global:datadir}/tab2.fits')
        config.set('cat3', 'ext', str(1))
        config.set('cat3', 'lon', 'RA')
        config.set('cat3', 'lat', 'DEC')
        config.set('cat3', 'frame', 'icrs')
        config.set('cat3', 'dist_threshold', str(0.2))

        config.write(conffile.open(mode='w', ensure=True))

        return conffile, config

    def test_cross_match(self, generate_xmatchs):

        conffile, config = generate_xmatchs

        config = parse_config(str(conffile))

        result = cross_match(config,
                             lonlat=[0.5 / 60, 0.5 / 60],
                             coordframe='icrs')

        assert result.keys() == dict(config['cat_fits']).keys()
        for cat in ['cat1', 'cat3']:
            assert len(result[cat]) == 1

        assert result['cat2'] is None

        assert result['cat1']['_sep'] == 0.0
        npt.assert_almost_equal(result['cat3']['_sep'], 0.1 * np.sqrt(2))

    def test_cat_fits(self, generate_xmatchs):

        conffile, config = generate_xmatchs
        cat_fits = parse_config(str(conffile))['cat_fits']

        coord_in = coord.SkyCoord(0.5, 0.5,
                                  unit=u.arcmin, frame='icrs')

        result = xmatch_fits(coord_in, cat_fits)
        assert result.keys() == dict(cat_fits).keys()
        for cat in ['cat1', 'cat3']:
            assert len(result[cat]) == 1

        assert result['cat2'] is None

        assert result['cat1']['_sep'] == 0.0
        npt.assert_almost_equal(result['cat3']['_sep'], 0.1 * np.sqrt(2))

    def test_xmatches(self, generate_xmatchs):

        conffile, config = generate_xmatchs
        cat_fits = parse_config(str(conffile))['cat_fits']

        coord_in = coord.SkyCoord([0.4, 0.5, 0.6], [0.5, 0.5, 0.5],
                                  unit=u.arcmin, frame='icrs')

        result = xmatch_fits(coord_in, cat_fits)
        npt.assert_array_equal(result['cat1']['index'], [0, 1, 2])
        npt.assert_array_almost_equal(result['cat1']['_sep'], 0)

        npt.assert_array_equal(result['cat2']['index'], [0, 2])
        npt.assert_array_almost_equal(result['cat2']['_sep'], 0.1)

        npt.assert_array_equal(result['cat3']['index'], [0, 1, 2])
        npt.assert_almost_equal(
            result['cat3']['_sep'], [0.1, 0.1 * np.sqrt(2), 0.1])

    def test_merge_tables(self, generate_xmatchs):

        conffile, config = generate_xmatchs
        cat_fits = parse_config(str(conffile))['cat_fits']

        coord_in = coord.SkyCoord(0.5, 0.5, unit=u.arcmin, frame='icrs')
        tab_in = Table()
        tab_in.add_column(Table.Column(name='index', data=[0]))

        matched_fits = xmatch_fits(coord_in, cat_fits)

        result = merge_tables(tab_in, matched_fits)
        keys = result.columns.keys()
        assert keys.sort() == ['index',
                               'cat1_RA', 'cat1_DEC',
                               'cat1_id', 'cat1_sep',
                               'cat2_id', 'cat2_sep',
                               'cat3_RA', 'cat3_DEC',
                               'cat3_id', 'cat3_sep'].sort()

    @pytest.mark.parametrize("filename",
                             ['result.txt',
                              'result.html',
                              'result.fits',
                              'result.csv'])
    def test_main(self, generate_xmatchs, filename):

        lon = 0.5 * u.arcmin
        lat = 0.5 * u.arcmin
        conffile, config = generate_xmatchs
        args = str(conffile) + \
            ' --lonlat %f %f' % (lon.to(u.deg).value,
                                 lat.to(u.deg).value) + \
            ' --coordframe icrs' + \
            ' --outdir %s' % os.path.dirname(str(conffile)) + \
            ' --filename %s' % filename

        result = main(args.split())
        assert os.path.exists(os.path.join(os.path.dirname,
                                           str(conffile)))
