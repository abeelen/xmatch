#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (c) 2016 IAS / CNRS / Univ. Paris-Sud
# LGPL License - see attached LICENSE file
# Author: Alexandre Beelen <alexandre.beelen@ias.u-psud.fr>

from .xmatch import *

r"""
 __   ____  __       _       _
 \ \ / /  \/  |     | |     | |
  \ V /| \  / | __ _| |_ ___| |__
   > < | |\/| |/ _` | __/ __| '_ \
  / . \| |  | | (_| | || (__| | | |
 /_/ \_\_|  |_|\__,_|\__\___|_| |_|
===================================

Cross Match
"""

__version__ = '0.1.1'
